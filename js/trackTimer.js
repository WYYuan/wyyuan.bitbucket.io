$(document).ready(function(){
	var eventNum = 1; //should be a count of the number of events
	var startTimes = []; //should be imbedded into something
	var clockRunning = [];
	

	//chooses the type of event
	$('body').on('click', '.setOptions', function(){
		var eventType = $(this).closest('div').find('input:checked').val();
		var eventName = $(this).closest('div').find('.eventName').val();

		if (eventName.length == 0){
			alert("You must submit an event name!");
			return;
		}

		eventNum += 1;
		//make a new "newEvent" tab
		$('.events').append("<div class = 'tabdivs event" + eventNum + "' event='event" + eventNum + "' eventnum="+ eventNum + "></div>");
		$('.tabList').append("<button class='tablinks newEvent' opens = 'event" + eventNum + "'>New Event</button>");
		var div = $(this).closest('.tabdivs');
		if (eventType == 'no lanes'){div.append(noLanesTable());}
		else if (eventType == 'lanes'){div.append(lanesTable(1));}
		else if (eventType == 'XC'){div.append(XCTable());}
		else if (eventType == 'field'){div.append(fieldTable());}
		else{div.append(fieldTable());}
		
		$(this).closest('div').clone().appendTo(".event" + eventNum);
		//clear the current tab and set its name
		$(this).closest('div').remove();
		
		$('.event' + eventNum).find('.eventNum').val("Event Name");
		//set current tab to that val
		$('.tablinks[opens=event'+ (eventNum - 1)+"]").html(eventName).trigger('click');
	});

	//builds the roster from text file
	$('#buildRoster').click(function(){
		var classes = ['id', 'name', 'grade', 'school'];
		var table = "<table class = rosters><th>ID</th><th>Name</th><th>Grade</th><th>School</th>";
		var data = $('textarea[name=roster]').val().split('\n');
		for (x in data){
			var row = data[x];
			if (row.replace("\t", "") == ""){
				continue;
			};
			cells = row.split(/[\t,]+/);
			
			table += "<tr>"
			for (y in cells){
				var cell = cells[y];
				table += "<td class = " + classes[y] + " value = '" + cell + "'>" + cell + "</td>";
			}
		}

		$('.rosterBuilder').empty().append(table);
	});

	//retrieves id's
	$('body').on('blur', '.rosterId', function(){
		if ($(this).val() != ""){
			var $targetRow = $('.id[value=' + $(this).val() + ']').closest('tr');
			var resultsRow = $(this).closest('tr');
			var school = $targetRow.find('.school').attr('value');
			var name = $targetRow.find('.name').attr('value');
			var grade = $targetRow.find('.grade').attr('value')
			resultsRow.find('.name').html(name);
			resultsRow.find('.school').html(school);
			resultsRow.find('.grade').html(grade);

			var type = $(this).closest('.finisherTable').attr('type');
			if ( type == 'XC'){
				event = $(this).closest('.tabdivs').attr('eventnum');
				
				var teamPlace = getTeamPlace(event, school);
				resultsRow.find('.teamPlace').html(teamPlace);
				populateTeamScore(event);
			}
			
		}
	});


	//============================
	function recalculateTeamScore(event){
		teamPlacesColl[event] = undefined;
		teamsAndFinishesColl[event] = undefined;
  
		$('.tabdivs.event' + event).find('.finisherTable').find('tr').each(function(){
			var school = $(this).find('.school').html(); 
			$(this).find('.teamPlace').text(getTeamPlace(event, school));
		});
		populateTeamScore(event);
	}



	function getTeamPlace(event, school){
		//for the recalculate function, taking into account the header rwo
		if (school == undefined){return ""; }
		if (teamPlacesColl[event] == undefined){teamPlacesColl[event] = 1;}
		if (teamsAndFinishesColl[event] == undefined){teamsAndFinishesColl[event] = [];}

		if (teamsAndFinishesColl[event][school] == undefined){
			teamsAndFinishesColl[event][school] = [teamPlacesColl[event]];
			teamPlacesColl[event] += 1;
			return teamPlacesColl[event] -1 ;
		}
		else if (teamsAndFinishesColl[event][school].length < 5){
			teamsAndFinishesColl[event][school].push(teamPlacesColl[event]);
			teamPlacesColl[event] += 1;
			return teamPlacesColl[event] -1;
		}
		else{
			return "";
		}
	}

	var teamPlacesColl = []
	var teamsAndFinishesColl = []

	function add(a,b){return a + b;}

	function populateTeamScore(event){	
		//sum the score, then multiply by 10^(5-n) where n is the size of the team
		//a team of 1 should almost always end last
		//make if not exist
		var keys = [];
		var scores = [];
		var teamSizes = [];

		var teamPlaceTable = "<br><br><br><table class = 'teamPlaceTable'><tr><th>Place<th>Score<th>Team Of<th>Team</tr>";
		for (var key in teamsAndFinishesColl[event]){
			keys.push(key);
			scoresArray = teamsAndFinishesColl[event][key];
		 	score = scoresArray.reduce(add, 0);
		 	teamSize = scoresArray.length;
		 	scores.push(score*100**(5-teamSize));
		 	teamSizes.push(teamSize);
		}

		var teamPosition = 1;
		while (scores.length > 0){
			score = Math.min.apply(Math, scores);
			var indexInArrays = scores.indexOf(score);
			var teamName = keys[indexInArrays];
			var teamSize = teamSizes[indexInArrays];
			keys.splice(indexInArrays, 1);
			scores.splice(indexInArrays, 1);
			teamSizes.splice(indexInArrays, 1);
			score = score / (100**(5-teamSize)); //reverse math transform
		 	teamPlaceTable += "<tr><td>" + teamPosition + "</td><td>" + score + 
		 		"</td><td>" + teamSize + "</td><td>" + teamName  + "</td>"; 
		 	teamPosition += 1;
		}
		$('.tabdivs.event' + event).find('.teamScoreDiv').empty().append(teamPlaceTable);
	}



	//adds event table for non laned events (no heats in the event)
	function XCTable(){
		var table = "<div class = timer> " +
						"<h1 class = 'timeDisplay'> 00:00</h1>"+
						"<span><button class = 'newFinisher' >New Finisher</button>"+
						"<h1> Finished so far: </h1><h1 class = numberFinished>0</h1></span>" + 
						"<button style='float:right' class='startStop'>Start/Stop</button>"+
					"</div>";
		table += "<table type='XC' class=finisherTable finishers = 1><tr><th>Place</th><th>Team Place</th>" +
			"<th>Name</th><th>Grade</th><th>School</th><th>Time</th><th>ID</th></tr></table><div class=teamScoreDiv></div>";
		return table;
	}

	//adds event table for non laned events (no heats in the event)
	function noLanesTable(){
		var table = "<div class = timer> " +
						"<h1 class = 'timeDisplay'> 00:00</h1>"+
						"<span><button class = 'newFinisher' >New Finisher</button>"+
						"<h1> Finished so far: </h1><h1 class = numberFinished>0</h1></span>" + 
						"<button style='float:right' class = 'startStop' >Start</button>"+
					"</div>";
		table += "<table class=finisherTable finishers = 1><tr><th>Place</th>" +
			"<th>Name</th><th>Grade</th><th>School</th><th>Time</th><th>ID</th></tr>";
		table += "</table>";
		return table;
	}

	//adds event table for field events
	function fieldTable(){
		var table = "<div><span><button class = 'newJumper' >New Jumper/Thrower</button></div>";
		table += "<div><span><button class = 'calculateJumper' >Tally results</button></div>";
		table += "<table class=finisherTable finishers = 1><tr><th>Place</th>" +
			"<th>Name</th><th>Grade</th><th>School</th><th>Best</th><th>ID</th><th>Mark 1</th><th>Mark 2</th><th>Mark 3</th></tr>";
		table += "</table>";
		return table;
	}

	//jumper find scores
	$('body').on('click', '.newJumper', function(){
		var eventnum = parseInt($(this).closest('.tabdivs').attr('eventnum'));
	});

	//find best score
	$('body').on('blur', '.jumpResult', function(){
		var row=$(this).closest('.individualFinisher');
		res = row.find('.jumpResult');
		var first=convertToInches(res[0].value);
		var second=convertToInches(res[1].value);
		var third=convertToInches(res[2].value);
		
		var bestScore = Math.max(first, second, third);
		var feet = Math.floor(bestScore/12);
		var inches = bestScore - feet*12;

		var textBest = feet + "' " + inches + '"'
		row.find('.time').text(textBest);
	})

	function convertToInches(text){
		if (text == "" || text.toUpperCase() == 'X'){
			return 0;
		}
		else{
			feet=parseInt(text.split('-')[0])
			inches=parseFloat(text.split('-')[1])
			return feet*12 + inches;
		}
	}

		

	//clock for current events
	$('body').on('click', '.startStop', function(){
		var eventnum = parseInt($(this).closest('.tabdivs').attr('eventnum'));
		if (startTimes[eventnum] == undefined){
			startTimes[eventnum] = new Date();
			setClock(eventnum);
		}	
		else{
			var status = confirm("Are you sure you would like to stop?");
			if (status == true){
				startTimes[eventnum] = undefined;
			}
		}
		$(this).blur();//if space hotkey is used after clickign start
	});

	function setClock(eventnum){
		if (startTimes[eventnum] == undefined){
			clearTimeout(clockRunning[eventnum]);
			$('.event' + eventnum).find('.timeDisplay').text('0:00.00');
		}
		else{
			var now = new Date();
			var secondsElapsed = now - startTimes[eventnum];

			$('.event' + eventnum).find('.timeDisplay').text(formatSeconds(secondsElapsed));
			clockRunVar = setTimeout(function(){setClock(eventnum)}, 100);
			clockRunning[eventnum] = clockRunVar;
		}	
	}

	function formatSeconds(milliseconds){
		var seconds = milliseconds/1000;
		var minutes = Math.floor(seconds /60);
		seconds -= minutes*60;
		seconds = parseFloat(seconds).toFixed(2);
		seconds < 10 ? seconds = "0" + seconds : seconds = seconds;
		return minutes + ":" + seconds;
	}

	//recalculate team score in the event of a typo
	$('body').on('click', '.recalculateTeamScore', function(){
		var event = $(this).closest('.tabdivs').attr('eventnum');
		recalculateTeamScore(event);
	});

		//adds finishers for non laned events
	$('body').on('click', '.newFinisher', function(){
		var $eventDiv = $(this).closest('.tabdivs');
		var time = $eventDiv.find('.timeDisplay').text();
		var table = $eventDiv.find('.finisherTable').last();
		var finishers = parseInt(table.attr('finishers'));
		var text = "<tr class = 'individualFinisher'>" + 
			"<td class = place>" + finishers + "</td>";

		var type = table.attr('type');
		if (type == 'XC'){
			text += "<td class = teamPlace></td>"
		}

		text += "<td class = name></td>" +
			"<td class = grade></td>" +
			"<td class = school></td>" + 
			"<td class = time>"+ time+ "</td>" + 
			"<td><input class = rosterId type = text style='width:100%'></input></td>"

		table.append(text);
		table.attr('finishers', finishers + 1);
		$(this).closest('.tabdivs').find('.numberFinished').html(finishers);
		$eventDiv.scrollTop($eventDiv.prop('scrollHeight'));

	});


	//adds contestant for field events
	$('body').on('click', '.newJumper', function(){
		var $eventDiv = $(this).closest('.tabdivs');
		var table = $eventDiv.find('.finisherTable').last();

		var text = "<tr class = 'individualFinisher'>" + 
			"<td class = place></td>";

		var type = table.attr('type');

		text += "<td class = name></td>" +
			"<td class = grade></td>" +
			"<td class = school></td>" + 
			"<td class = time></td>" + //becomes best on the table
			"<td><input class = rosterId type = text style='width:100%'></input></td>"+
			"<td><input class = jumpResult name=first type = text style='width:100%'></input></td>"+
			"<td><input class = jumpResult name=second type = text style='width:100%'></input></td>"+
			"<td><input class = jumpResult name=third type = text style='width:100%'></input></td></tr>";

		table.append(text);
	});


	//adds event table for laned events (track events)
	function lanesTable(heat){
		var table = "<h1>Heat" + heat +  "</h1>" + 
						"<div class = timer> " +
						"<h1 class = 'timeDisplay'> 00:00</h1>"+
						"<span><button class = 'newFinisher' >New Finisher</button>"+

						"<button style='float:right' class = 'startStop' >Start</button>"+
					"</div>";
		table += "<table heat=" + heat + " class=finisherTable finishers = 1><tr><th>Place</th>" +
			"<th>Name</th><th>Grade</th><th>School</th><th>Time</th><th>ID</th></tr>";
		table += "</table><button style='float:right' class = 'newHeat' >New Heat</button>";
		return table;
	}

	$('body').on('click', '.newHeat', function(){
		var eventnum = parseInt($(this).closest('.tabdivs').attr('eventnum'));
		startTimes[eventnum] = undefined;
		var $containerDiv = $(this).closest('.tabdivs');
		$containerDiv.find('.timer').remove();
		$containerDiv.find('.newHeat').remove();

		nextHeat = parseInt($containerDiv.find('.finisherTable').last().attr('heat'))+ 1;
		$containerDiv.append(lanesTable(nextHeat));
	});



	//changing tabs
	$('body').on('click', '.tablinks', function(){
		if ($(this).html() == "New Event" && eventNum > 1){
			$('.saveText').click()
		}


		$('.tablinks').removeClass('active');
		$(this).addClass('active');
		var tabclick = $(this).attr('opens');
		$('.tabdivs').addClass('hidden');
		$('.'+ tabclick).removeClass('hidden');
	});

	//deletes text from initial text box on click for entry
	$('textarea[name=roster]').focus(function(){
		if ($(this).val().indexOf('format') >= 0){
			$(this).val("");
		}
	});

	$('textarea[name=roster]').blur(function(){
		if ($(this).val() == ""){
			$(this).val("Paste the rosters in here the format is id | name | grade | school (tab or comma delimited)");
		}
	});

	//hotkeys
	$('body').on('keypress', function(event){
		if (event.which == 13){
			event.preventDefault();
			return;
		}
		//binds start/stop to the control plus space keys
		if (event.which == 32 && event.ctrlKey == true){ //Control space
			event.preventDefault();
			$('.tabdivs').not('.hidden').find('.startStop').click();
		}

		else if (event.which == 32){ //space
			event.preventDefault();
			$('.tabdivs').not('.hidden').find('.newFinisher').click();
		}
	})

	$('body').on('click', '.saveText', function(){
		for (i = 0; i<= eventNum; i++){
			recalculateTeamScore(i);
		}

		fileName = $('.meetName').val()
		if (fileName.length == 0){
			alert("Please enter a meet name!");
			return;
		}

		var text = "";
		$('.newEvent').each(function(){

			var opens = $(this).attr('opens');
			text += '===================================\n';
			text += $(this).html() + "\n\n"; //event name


			//individual results
			$('.' + opens).find('.finisherTable').each(function(){
				if ($(this).attr('heat') != undefined){
					heat = $(this).attr('heat');
					text += 'Heat ' + heat + "\n";
				};

				text += "Place\tname\tgrade\tschool\ttime\n";
				$(this).find('tr').each(function(){//skip the header row
					if ($(this).html().indexOf('<th>')< 0){
						place = $(this).find('.place').html()
						name = $(this).find('.name').html()
						grade = $(this).find('.grade').html()
						school = $(this).find('.school').html()
						time = $(this).find('.time').html()
						text += place + "\t" +name + "\t" +grade + "\t" +
							school + "\t" +time
						text += '\n'
					}
				});	
			});

			//team results
			$('.' + opens).find('.teamPlaceTable').each(function(){
				text += "\nPlace\tScore\tTeam Of\tTeam\n";
				$(this).find('tr').each(function(){//skip the header row
					if ($(this).html().indexOf('<th>')< 0){
						place = $(this).find('td:nth-child(1)').html();
						score = $(this).find('td:nth-child(2)').html();
						teamOf = $(this).find('td:nth-child(3)').html();
						team = $(this).find('td:nth-child(4)').html();

						text += place + "\t" + score + "\t" + teamOf + "\t" + team;
						text += '\n'
					}
				});	
			});



			text += "\n"
		});



		text2 = new Blob([text], {
        	type: "text/plain;charset=utf-8;",
        });
        text.replace('undefined', '');
        saveAs(text2, fileName + '.txt');
    });

    a = function(){
		var url = window.location.href;
		var roster = url.split('roster=')[1];
		$.ajax({
			url:roster + ".txt",
			dataType: "text", 
			async: false,
			success: function(data){
				$('textarea[name=roster]').empty().append(data);
				$('#buildRoster').trigger('click');		
			},
		});

	}
	a();

});